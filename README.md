# Shopfiy Slate Framework

1. [Requirements](#requirements)

2. [Getting Started](#getting-started)

3. [Commands](#commands)

4. [Shopify guidelines](#shopify-guidelines) 

5. [Javascript guidelines](#javascript-guidelines)

6. [Themes](#themes)

7. [Code Linting](#code-linting)

8. [Framework Guide - Grid System](#framework-guide)

7. [Git Workflow](#git-workflow)

## Getting started
Start project - `npm run start`

Test project before committing - `npm run test`

## Commands

Test - Runs all tests and code linters

`npm run test`

Lint CSS - Checks scss for errors and best practices.

`` npm run lint:css``

Lint JS - Checks js for errors and best practices.

`` npm run lint:js``

Fix CSS - Checks scss for errors and best practices. Automatically fixes simple errors like line endings

`` npm run fix:css``

Fix jS - Checks js for errors and best practices. Automatically fixes simple errors.

`` npm run fix:js``

## Shopify guidelines

Regular images - use ``src/snippets/responsive-image.liquid``

Background images - ``src/snippets/responsive-bg-image.liquid``

Cart - use [CartJS + Rivets](https://cartjs.org/)

Drawer - use [pure-drawer](https://github.com/mac81/pure-drawer)

## Javascript guidelines

1. Keep code for section files (ex. header.liquid) in sections folder with matching name
2. Keep code for templates (ex. product.liquid) in templates folder with matching name
3. Keep code for layout (ex. theme.liquid, page.liquid, etc) in layout folder with matching name
4. All other code can go in modules

## Themes

**Live/Production** - Live theme

Branches: master

**Staging** - Used for CI deployments and testing

Branches: develop, bugfix, hotfix, releas

**Development** - Use this theme for work

Branches: feature

## Code Linting
This project uses stylelint and eslint for checking css and js. please run linters and make sure there are no errors 
before committing your code

**Do not edit .stylelintrc or .eslintrc files**

![linter](https://github.com/stylelint/stylelint/raw/master/example.png?raw=true)

- [style lint](https://stylelint.io/)
- [prettier](https://github.com/prettier/stylelint-prettier)
- [ESLint](https://eslint.org/)

### Important CSS Rules
1. All files must use unix (LF) line endings
2. No more than 1 blank line outside of rules
3. No blank lines inside rules
4. `!important` is not allowed

## Framework Guide

### Two Dashes style
block-name__elem-name--mod-name--mod-val

- Names are written in lowercase Latin letters.

- Words within the names of BEM entities are separated by a hyphen (-).

- The element name is separated from the block name by a double underscore (__).

- Boolean modifiers are separated from the name of the block or element by a double hyphen (--).

- The value of a modifier is separated from its name by a double hyphen (--).

### Responsive Mixins
Use the media-query mixin provided for all breakpoints, example:

```scss
@include media-query($medium-up) {
  // Responsive styles here
}
```

More details:

```scss
// Grid
$grid-medium: 768px;
$grid-large: 960px;
$grid-widescreen: 1200px;
$grid-max-width: 1180px;
$grid-gutter: 30px;

// Breakpoints used with the media-query mixin
$small: "small";
$medium: "medium";
$medium-down: "medium-down";
$medium-up: "medium-up";
$large: "large";
$large-down: "large-down";
$large-up: "large-up";
$widescreen: "widescreen";

$breakpoints: (
        $small "(max-width: #{$grid-medium - 1})",
        $medium "(min-width: #{$grid-medium}) and (max-width: #{$grid-large - 1})",
        $medium-down "(max-width: #{$grid-large - 1})",
        $medium-up "(min-width: #{$grid-medium})",
        $large "(min-width: #{$grid-large}) and (max-width: #{$grid-widescreen - 1})",
        $large-down "(max-width: #{$grid-widescreen - 1})",
        $large-up "(min-width: #{$grid-large})",
        $widescreen "(min-width: #{$grid-widescreen})"
);
```

### Flexbox Grid
In this kind of layout, the width of the columns are implicit, so columns will adjust automatically to be equal width to their adjacent columns.
```html
          <!-- .container is main centered wrapper -->
          <div class="container">
            <!--  Defining a .flex-grid__col should always be wrapped by .flex-grid -->
            <div class="flex-grid">
              <div class="flex-grid__col">Auto</div>
              <div class="flex-grid__col">Auto</div>
              <div class="flex-grid__col">Auto</div>
              <div class="flex-grid__col">Auto</div>
            </div>
            <!-- .flex-grid__col--sm will create a column half the width of its adjacent columns -->
            <div class="flex-grid flex-grid--md">
              <div class="flex-grid__col flex-grid__col--sm">flex-grow: 2</div>
              <div class="flex-grid__col">flex-grow: 4</div>
            </div>
            <!-- .flex-grid__col--fixed-width will create a column fixed at 200px. Adjust the default fixed width in _variables.scss -->
            <div class="flex-grid flex-grid--md">
              <div class="flex-grid__col flex-grid__col--fixed-width">fixed-width: 200px</div>
              <div class="flex-grid__col">flex-grow: 4</div>
            </div>
            <!-- .flex-grid__col--lg will create a column twice the width of the default columns -->
            <div class="flex-grid flex-grid--lg">
              <div class="flex-grid__col flex-grid__col--lg">flex-grow: large</div>
              <div class="flex-grid__col">flex-grow: default</div>
            </div>
          </div>
```

### Grid System - 12 column fluid grid
All columns become full width below 540px

```html
 <!-- .container is main centered wrapper -->
          <div class="container">

            <!-- columns must be the immediate child of a .row -->
             <div class="row">
              <div class="col col--2">col--2</div>
              <div class="col col--10">col--10</div>
            </div>

            <!-- just use the class 'col' and 'col--n', where n is the number of columns -->
            <div class="row">
              <div class="col col--3">col--3</div>
              <div class="col col--9">col--9</div>
            </div>

            <!-- there are a few shorthand columns widths as well -->
            <div class="row">
              <div class="col col--1-3">1/3</div>
              <div class="col col--2-3">2/3</div>
            </div>
            <div class="row">
              <div class="col--1-2 col">1/2</div>
              <div class="col--1-2 col">1/2</div>
            </div>

          </div>

          <!-- Note: columns can be nested, but it's not recommended since BEMSkel's grid has %-based gutters, meaning a nested grid results in variable with gutters (which can end up being *really* small on certain browser/device sizes) -->
```

### Responsive Grid System - < 768px
Grid system is located in `styles/tools/_grid.scss`

.small + --width

.small--1 = Column with 1/12 width
```scss
.small--1
.small--2
.small--3
```

```html
<!-- .container is main centered wrapper -->
<div class="container">
    <!-- columns must be the immediate child of a .row -->
     <div class="row">
      <div class="col col--2 small--12">small--12</div>
      <div class="col col--10 small--12">small--12</div>
    </div>
</div>
```

## Git Workflow

Use Gitflow workflow for all changes. More details:
[Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

### Branches

**Development branch (develop)**

Usually the integration branch for feature work and is often the default branch or a named branch. For pull request workflows, the branch where new feature branches are targeted.

**Production branch (master)**

Used for deploying a release. Branches from, and merges back into, the development branch. In a Gitflow-based workflow it is used to prepare for a new production release. 

**Feature branch (feature/)**

Used for specific feature work or improvements. Generally branches from, and merges back into, the development branch, using pull requests.

**Release branch (release/)**

Used for release tasks and long-term maintenance versions. They branch from, and merge back into, the development branch.

**Bugfix branch (bugfix/)**

Typically used to fix Release branches.

**Hotfix branch (hotfix/)**

Used to quickly fix a Production branch without interrupting changes in the development branch. In a Gitflow-based workflow, changes are usually merged into the production and development branches.

