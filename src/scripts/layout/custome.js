// import $ from 'jquery';
import slick from 'slick-carousel';
export default () => {
    ((custom, $) => {
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $('body');
        };
        const bindUIActions = () => {
            $(".toggle-btn").click(function () {
                $(".mobile-menu").css({
                    left: "0%"
                });
            });
            $(".close-btn").click(function () {
                $(".mobile-menu").css({
                    left: "-100%"
                });
            });

            $('.mobile-ul li a').click(function(){
              $(this).next().slideToggle();
              $(this).toggleClass("active-main");
            });
            $('.submenu_ul li').click(function(){
              $(this).next().slideToggle();
              $(this).toggleClass("active-main");
            });
            $(document).ready(function () {
              $('.show_btn').click(function(){
                $('#fillter_main').show();
                $(this).hide();
                $(this).next().show();
              });
              $('.hide_btn').click(function(){
                $('#fillter_main').hide();
                $(this).hide();
                $(this).prev().show();
              });
            });

            $(document).ready(function () {
                $('.view-product').click(function(){
                    $( ".view-product" ).each(function() {
                        $(this).children().first().css("opacity",".2");
                      });
                    $(this).children().first().css("opacity","1");
                    var all="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xl-4 col-lg-4 col-xl-3 col-lg-3 col-md-4 col-sm-4"; 
                    var c = $(this).data('class');
                    $(".product-main").each(function() {
                        $(this).removeClass(all);
                    });
                    if(c.indexOf('12') != -1){
                        $(".product-main").each(function() {
                            $(this).addClass(c);
                            $(this).find('.product_box').addClass('row p-1 mx-sm-2 my-sm-2 align-items-center');
                            $(this).find('.product_img_box').addClass("col-md-4 col-sm-4");
                            $(this).find('.product_info').addClass("col-md-8 col-sm-8");
                            $(this).find('.product_info .description').removeClass("d-none");
                            $(this).find('.product_info .description').addClass("d-block");
                        });  

                    }else{
                    $(".product-main").each(function() {
                        $(this).addClass(c);
                        $(this).find('.product_box').removeClass('row p-1 mx-sm-2 my-sm-2 align-items-center');
                        $(this).find('.product_img_box').removeClass("col-md-4 col-sm-4");
                        $(this).find('.product_info').removeClass("col-md-8 col-sm-8");
                        $(this).find('.product_info .description').addClass("d-none");
                        $(this).find('.product_info .description').removeClass("d-block");
                    });
                    }
                });  
                $(".add-accessories").click(function(){
                  $(".popup-wrapper").each(function(){
                  $(this).removeClass("popup-show");
                  });
                  $(".popup-wrapper-"+$(this).data("title")).addClass("popup-show");
                });
                $(".close").click(function(){
                  $(".popup-wrapper").each(function(){
                   $(this).removeClass("popup-show");
                   });
                 });
                $('.slider_wrapper_all').slick({
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                   responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow:2,
                  slidesToScroll: 2
                }
              }]
                });
                $('.slider_wrapper_modling').slick({
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                   responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }]
                });
                $('.slider_wrapper_adhesives').slick({
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                   responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }]
                });
                $('.slider_wrapper_underlay').slick({
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 3,
                   responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }]
                });
                $('.zoom').each(function(index){
                    $(this).zoom();             

                });
                $('.featured_brands_slider').slick({
                    infinite: true,
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    arrows:true,
                    dots:true,
                    responsive: [
                        {
                          breakpoint: 767,
                          settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            arrows:false
                        }
                        },
                        {
                          breakpoint: 600,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows:false
                          }
                        },
                        {
                          breakpoint: 480,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows:false
                          }
                        }
                      ]
                  });
                  $('.pagination-count').change(function(){
                    var ele = document.getElementsByName('pagination_value'); 
                           for(i = 0; i < ele.length; i++) { 
                               if(ele[i].checked) {
                                       console.log(ele[i].value);
                                        $.ajax({
                                       type: "POST",
                                       url: '/cart.js',
                                       data: {"attributes[pagination]": ele[i].value}, 
                                       success: function(d){
                                       window.location.reload(); 
                                       },
                                       dataType: 'json'
                                       });
                               }
                           } 
                   });
                  $('.blog_slider').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows:true,
                    dots:true,
                    responsive: [
                        {
                          breakpoint: 767,
                          settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            arrows:false
                        }
                        },
                        {
                          breakpoint: 600,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows:false
                          }
                        },
                        {
                          breakpoint: 480,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows:false
                          }
                        }
                      ]
                  });
         

            });



            //form login
            $("#forget").click(function () {
                $(".lgn").hide();
                $("#recover").show();
            });
            $(".cnl").click(function () {
                $("#recover").hide();
                $(".lgn").show();
            });

            //PDP Qty Selector
            $("button.qty-btns").on("click", function (event) {
                event.preventDefault();
                var container = $(event.currentTarget).closest('[data-qtyContainer]');
                var qtEle = $(container).find('[data-qty]');
                var currentQty = $(qtEle).val();
                var qtyDirection = $(this).data("direction");
                var newQty = 0;

                if (qtyDirection == "1") {
                    newQty = parseInt(currentQty) + 1;
                } else if (qtyDirection == "-1") {
                    newQty = parseInt(currentQty) - 1;
                }

                if (newQty == 1) {
                    $(".decrement-quantity").attr("disabled", "disabled");
                }
                if (newQty > 1) {
                    $(".decrement-quantity").removeAttr("disabled");
                }

                if (newQty > 0) {
                    newQty = newQty.toString();
                    $(qtEle).val(newQty);
                } else {
                    $(qtEle).val("1");
                }
            });

            $(document).ready(function () {
                $("#quickby").click(function () {
                    addtocart = $('#AddToCartForm');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/cart/add.js',
                        data: jQuery(addtocart).serialize(),
                        dataType: 'json',
                        success: function (cart) {
                            $.ajax({
                                url: '/cart',
                                success: function success(data) {
                                    window.location.href = "/checkout";
                                }
                            });
                        }
                    });
                });
            });
            $(document).ready(function(){
                $('.view-more').click(function(){
                  $(this).hide();
                  $('.main-row').css({ height: "auto" });
                  $('.view-less').css({ display: "block" });
                });
                $('.view-less').click(function(){
                  $(this).hide();
                  $('.main-row').css({ height: "205px" });
                  $('.view-more').css({ display: "block" });
                });
                var favorite = [];
                $.each($("input[name='diameter']:checked"), function(){
                favorite.push($(this).val());
                });
                if(favorite.length>0){
                $('.tags').append('<label>Diameter : </label> <span class="badge badge-pill badge-dark"> ' + favorite.join(", ")+'</span>');
                }
                var favorite = [];
                $.each($("input[name='height']:checked"), function(){
                favorite.push($(this).val());
                });
                if(favorite.length>0){
                $('.tags').append('<br><label>Height : </label> ');
                $.each(favorite, function( index, value ) {
                    $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
                  });
                  
            }
            var favorite = [];
            $.each($("input[name='width']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label>Width : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }
            var favorite = [];
            $.each($("input[name='color']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label>Color : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }
            var favorite = [];
            $.each($("input[name='tire_type_1']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label>Tier Type : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }


            var favorite = [];
            $.each($("input[name='treadwear']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Treadwear : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }

            var favorite = [];
            $.each($("input[name='speedrating']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Speed Rating : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }

            var favorite = [];
            $.each($("input[name='loadrange']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Load Range : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }

            
            var favorite = [];
            $.each($("input[name='loadindex']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Load Index : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }
            var favorite = [];
            $.each($("input[name='brand']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Brand : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }

            var favorite = [];
            $.each($("input[name='modal']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Modal : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }   
            var favorite = [];
            $.each($("input[name='year']:checked"), function(){
            favorite.push($(this).val());
            });
            if(favorite.length>0){
            $('.tags').append('<br><label> Year : </label> ');
            $.each(favorite, function( index, value ) {
                $('.tags').append(' <span class="badge badge-pill badge-dark"> ' + value +'</span>');
              });
            }     
        });

            $(document).ready(function(){
              // Add minus icon for collapse element which is open by default
              $(".collapse.show").each(function(){
                $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
              });
              
              // Toggle plus minus icon on show hide of collapse element
              $(".collapse").on('show.bs.collapse', function(){
                $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
              }).on('hide.bs.collapse', function(){
                $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
              });
              $(".coll-filter-stock").change(function(){
                var v=$(this).val();
                if(v == "Instock" )
                {
                   $(".available-stk").show();
                   $(".notavailable-stk").hide();
                }else if(v == "Outofstock" ){
                  $(".available-stk").hide();
                  $(".notavailable-stk").show();

                }
              });
            });
            
            //quiz section
            $(document).ready(function(){
                var current_fs, next_fs, previous_fs; //fieldsets
                var opacity;
                
                $(".next").click(function(){
                
                  current_fs = $(this).parent();
                  console.log("current_fs",current_fs);
                  next_fs = $(this).parent().next();
                
                //Add Class Active
                // $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;
                
                current_fs.css({
                'display': 'none',
                'position': 'relative'
                });
                next_fs.css({'opacity': opacity});
                },
                duration: 600
                });
                });
                  function next(val,next_val){
                    current_fs = val;
                    next_fs = next_val;
                
                    //Add Class Active
                    // $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                      step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;
                
                        current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                        });
                        next_fs.css({'opacity': opacity});
                      },
                      duration: 600
                    });
                  }
                $(".previous").click(function(){
                
                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();
                
                //Remove class active
                // $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                
                //show the previous fieldset
                previous_fs.show();
                
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;
                
                current_fs.css({
                'display': 'none',
                'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
                },
                duration: 600
                });
                });
                
                $('.radio-group .radio').click(function(){
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
                });
                
                $(".submit").click(function(){
                return false;
                })
                var radioValue,year, model,ring;
                var currant,next_val;
                  $("input[name='tyre_type']").click(function(){
                  radioValue = $("input[name='tyre_type']:checked").val();
                  $("#brand_next").prop('disabled', false);
                  var brand_val = [];
                  $( ".year_div").each(function( index , data ) {
                    var value = $(data).data("brand");
                    brand_val.push(value);
                    if(radioValue == $(data).data("brand")){
                        $(data).removeClass("d-none");
                    }else {
                      $(data).addClass("d-none");
                    }
                  });
                    
                  currant = $(this).closest("fieldset");
                    next_val = $(this).closest("fieldset").next();
                    next(currant,next_val);
                    test_next();
                  
                  
                  function test_next() {
                    var f = 0;
                    $(".year_div").each(function(i,data){
                      var new_value = $(this).data("brand");
                      if(radioValue == new_value){
                        f = 1;
                      }
                    });
                    
                    if(f == 0) {
                      $("#year_btn").click();
                    }
                  }
                  $("#cheack_pre").click(function(){
                    test_pre();
                  });
                  
                  function test_pre(){
                    var f = 0;
                    $(".year_div").each(function(i,data) {
                      var new_value = $(this).data("brand");
                      if(radioValue == new_value){
                        f = 1;
                      }
                    });
                    
                    if(f == 0) {
                      $("#pre_btn").click();
                    }
                  }
                  
                  $( ".model_div").each(function( index , data ) {
                    var value = $(data).data("brand");
                //     console.log(value);
                    if(radioValue == $(data).data("brand")){
                     $(data).removeClass("d-none");
                    }else {
                      $(data).addClass("d-none");
                    }
                  });
                });
                
                  
                  $("input[name='btn_year']").click(function(){
                    currant = $(this).closest("fieldset");
                    next_val = $(this).closest("fieldset").next();
                    next(currant,next_val);
                     year = $("input[name='btn_year']:checked").val();
                    $("#year_btn").prop('disabled', false);
                    console.log(year);
                  });
                  
                  $("input[name='model_div']").click(function(){
                    currant = $(this).closest("fieldset");
                    next_val = $(this).closest("fieldset").next();
                    next(currant,next_val);
                     model = $("input[name='model_div']:checked").val();
                    $("#model_btn").prop('disabled',false);
                    console.log(model);
                    
                  });
                  $("input[name='btn_ring']").click(function(){
                    ring = $("input[name='btn_ring']:checked").val();
                    $("#finish_btn").prop('disabled',false);
                    console.log(ring)
;
                    $("#finish_btn").click();
                  });
                  $("#finish_btn").click(function(){
                    localStorage.setItem("Brand", radioValue );
                    localStorage.setItem("Year", year );
                    localStorage.setItem("Model", model );
                    localStorage.setItem("Ring", ring );
                    $(".close").click();
                    if(typeof(year) == 'undefined'){
                      window.location.href = 'https://wheelsbelowretail.com/collections/result/'+ radioValue + '+' + model + '+'+ ring;
                    }
                    else{ + '+' + model + '+' + ring
                    window.location.href = 'https://wheelsbelowretail.com/collections/result/'+ radioValue + '+' + year + '+'+ model + '+' + ring;
                        }
                  });
                });
            // $(document).ready(function () {
            //   $('#AddToCart').on('click', function (event) {
            //     event.preventDefault();
            //     const $form = $(this).closest('form');
            //     Shopify.queue = [];
            //     const variantID = $form.find('[name="id"]').val();
            //     let Qty = parseInt($('[name="quantity"]').val());
            //     Shopify.queue.push({
            //       variantId: variantID,
            //       quantity: Qty
            //     });
            //     $('[data-queue]').each(function () {
            //       if ($(this).find('input[type="checkbox"]').prop('checked') == true) {
            //         let variantID = $(this).find('input[type="checkbox"]').val();
            //         Qty = 1;
            //         let properties = {};
            //         properties["Addons"] = true;

            //         Shopify.queue.push({
            //           variantId: variantID,
            //           quantity: Qty,
            //           properties: properties
            //         });
            //       }
            //     })
            //     console.log(Shopify.queue)
            //     Shopify.moveAlong();
            //   })

            //   Shopify.moveAlong = function () {
            //     if (Shopify.queue.length) {
            //       var request = Shopify.queue.shift();
            //       Shopify.addItem(request.variantId, request.quantity, request.properties, Shopify.moveAlong);
            //     } else {
            //       //Shopify.AjaxifyCart.init();
            //       document.location.href = '/cart';
            //     }
            //   };

            //   Shopify.addItem = function (id, qty, properties, callback) {
            //     var params = {
            //       quantity: qty,
            //       id: id
            //     };
            //     if (properties != false) {
            //       params.properties = properties;
            //     }
            //     $.ajax({
            //       type: 'POST',
            //       url: '/cart/add.js',
            //       dataType: 'json',
            //       data: params,
            //       success: function () {
            //         if (typeof callback === 'function') {
            //           callback();
            //         }
            //       },
            //       error: function () {}
            //     });
            //   }
            // });

            // $(".add-cart").click(function () {
            //     var addcart = $(this).data("attr");
            //     console.log(addcart);
            //     addtocart('#AddToCartForm-' + addcart);
            // });

            function addtocart(addtocart) {
                jQuery.ajax({
                    type: "POST",
                    url: "/cart/add.js",
                    data: jQuery(addtocart).serialize(),
                    dataType: "json",
                    success: function (cart) {
                        // CartJS.render(null, cart);
                        // cartDrawer.offcanvas('open');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseJSON.description);
                    }
                });
            }

        };
        custom.init = () => {
            cacheDom();
            bindUIActions();
        };

    })(window.custom = window.custom || {}, $);
}