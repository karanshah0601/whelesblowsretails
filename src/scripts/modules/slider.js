// import $ from 'jquery';
import slick from 'slick-carousel';

export default () => {
    (function(Slider, $) {

        const $dom = {};
        const arrow = '<svg fill="#959595" viewBox="0 0 129 129"><path d="M121.3 34.6c-1.6-1.6-4.2-1.6-5.8 0l-51 51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8 0-1.6 1.6-1.6 4.2 0 5.8l53.9 53.9c.8.8 1.8 1.2 2.9 1.2 1 0 2.1-.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2.1-5.8z"/></svg>'
        const cacheDom = () => {

            $dom.bannerSlider = $(".banner-slider");
            $dom.proindexSlider = $(".index-pro-slider");
            $dom.testimonialSlider = $(".testimonial-slide");
            $dom.pdpSlider = $(".product-slider");
            $dom.pdpSliderFor = $(".pro-nav-ul");
            $dom.relatedSlider = $(".related-product-slider");
            $dom.formulaSlider = $(".formula-slider");
            $dom.proSlider = $(".in-pro-slider");
            $dom.brandSlider = $(".brand_slider");
        };
        const proSlidersInit = () => {
            const proSliderOptions = {
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            autoplay: true,
                        }
                    }
                ]
            }
            $dom.proSlider.slick(proSliderOptions);
        };

        const bannerSlidersInit = () => {
            const bannerSliderOptions = {
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                arrows: false
            }
            $dom.bannerSlider.slick(bannerSliderOptions);
        };
        const proindexSlidersInit = () => {
            const proindexSliderOptions = {
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            autoplay: true,
                        }
                    }
                ]
            }
            $dom.proindexSlider.slick(proindexSliderOptions);
        };
        const testimonialSlidersInit = () => {
            const testimonialSliderOptions = {
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                arrows: false,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }]
            }
            $dom.testimonialSlider.slick(testimonialSliderOptions);
        };
        const brandSlidersInit = () => {
            const brandSliderOptions = {
                infinite: true,
                slidesToShow: 6,
                slidesToScroll: 2,
                autoplay: false,
                autoplaySpeed: 2000,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 5
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 2
                        }
                    }
                ]
            }
            $dom.brandSlider.slick(brandSliderOptions);
        };
        const SlidersInit = () => {
            const pdpSliderOptions = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                fade: true,
                asNavFor: $dom.pdpSliderFor
            }

            const pdpSliderSliderOptions = {
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: $dom.pdpSlider,
                dots: false,
                vertical: true,
                centerMode: true,
                arrows: false,
                focusOnSelect: true
            }

            $dom.pdpSlider.slick(pdpSliderOptions);
            $dom.pdpSliderFor.slick(pdpSliderSliderOptions);
        };

        const relatedSlidersInit = () => {
            const relatedSliderOptions = {
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                arrows: true,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            }
            $dom.relatedSlider.slick(relatedSliderOptions);
        };

        const formulaSlidersInit = () => {
            const formulaSliderOptions = {
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            }
            $dom.formulaSlider.slick(formulaSliderOptions);
        };

        Slider.init = function() {
            cacheDom();
            proSlidersInit();
            bannerSlidersInit();
            proindexSlidersInit();
            testimonialSlidersInit();
            SlidersInit();
            relatedSlidersInit();
            formulaSlidersInit();
            brandSlidersInit();
        };

    }(window.Slider = window.Slider || {}, $, undefined));
}